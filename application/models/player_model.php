<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Player_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		$this -> load -> config('tables', TRUE);
		$this -> tablas = $this -> config -> item('tablas', 'tables');
	}

	public function insertPlayer($arrInsert){
		$this->db->insert($this->tablas['jugador'],$arrInsert);
		return $this->db->insert_id();
	}

	public function getPlayer($idPlayer)
	{
		$query = $this->db->get_where($this->tablas['jugador'],array('id'=>$idPlayer));
		return ($query->num_rows()==1)?$query->row():null;
	}

	public function getPlayers()
	{
		$query = $this->db->get($this->tablas['jugador']);
		return ($query->num_rows()>0)?$query->result():null;
	}

}

/* End of file player_model.php */
/* Location: ./application/models/player_model.php */