<?php

if (!defined('BASEPATH'))

	exit('No direct script access allowed');
class Usuario_model extends CI_Model {

	var $tablas = array();
	function __construct() {

		parent::__construct();
		$this->load->config('tables', TRUE);
		$this->tablas = $this->config->item('tablas', 'tables');
		$this->load->model('auth_model');
	}

	function getNewConfirmationCode($emailUsuario) {

		//obtiene un nuevo código de confirmación, para cambiar contraseña o para activar

		return substr(strtoupper(sha1(substr(md5(uniqid(rand(), true)), 0, 35) . md5($emailUsuario))), 0, 25);
	}

	function registrarUsuario($data) {

		//registra un usuario

		$data['authKey'] = $this->auth_model->getNewAuthKey($data['emailUsuario'], 20);
		$data['contrasenaUsuario'] = $this->auth_model->hashPassword($data['contrasenaUsuario'], null);
		$this->db->insert($this->tablas['usuario'], $data);
		return $this->db->insert_id();
	}

	function is_there_activation_code($activationCode) {

		$this->db->where('confirmationCode', $activationCode);
		$query = $this->db->get($this->tablas['usuario']);
		if ($query->num_rows == 1)

			return $query;
		return null;
	}

	function activar($activationCode) {

		//activa un usuario, devuelve 1: activado, 0:usuario ya activado, -1: codigo no existe

		//si el usuario ya esta activo y se intenta activar nuevamente, cambia el confirmation code nuevamente

		$activacion = $this->is_there_activation_code($activationCode);
		if ($activacion != null) {

			$row = $activacion->row();
			if ($row->status == '1')

				return 0;
			$this->db->where('idUsuario', $row->idUsuario);
			$this->db->update($this->tablas['usuario'], array('status' => 1, 'confirmationCode' => $this->getNewConfirmationCode($row->emailUsuario))); ;
			$this->load->model('auth_model');
			$this->auth_model->iniciarsesion($row, null);
			return 1;
		} else {

			return -1;
		}

	}

	/*

	 * Información de la cuenta

	 * */

	function getMyInfo($idUsuario) {

		$this->db->select('idUsuario, idRol, nivel, status, usuario, emailUsuario, nombre, apellidos');
		$this->db->where('idUsuario', $idUsuario);
		$query = $this->db->get($this->tablas['usuario']);
		if ($query->num_rows() == 1)

			return $query;
		return null;
	}

	function updateUser($idUsuario,$arrUpdate){
		$this->db->where('idUsuario',$idUsuario);
		return $this->db->update($this->tablas['usuario'],$arrUpdate);
	}

	/*

	 * Modificar información de la cuenta

	 * */

	function cambiarContrasena($contrasenaActual, $contrasenaUsuario, $idUsuario, $admin) {

		//obvio

		if (!$admin) {

			$this->load->model('auth_model');
			$this->db->select('contrasenaUsuario');
			$this->db->where('idUsuario', $idUsuario);
			$query = $this->db->get($this->tablas['usuario']);
			if ($query->num_rows() == 1) {

				$row = $query->row();
				if ($this->auth_model->hashPassword($contrasenaActual, substr($row->contrasenaUsuario, 0, 10)) == $row->contrasenaUsuario) {

					$arrNewPass = array('contrasenaUsuario' => $this->auth_model->hashPassword($contrasenaUsuario));
					$this->db->where('idUsuario', $idUsuario);
					$this->db->update($this->tablas['usuario'], $arrNewPass);
					return true;
					// die('cambio');
				} else {

					return false;
					// die('no coincide pass');
				}

			} else {

				return false;
				// die('no usuario');
			}

		} else {

			$this->load->model('auth_model');
			$this->db->select('contrasenaUsuario');
			$this->db->where('idUsuario', $idUsuario);
			$query = $this->db->get($this->tablas['usuario']);
			if ($query->num_rows() == 1) {

				$row = $query->row();
				$arrNewPass = array('contrasenaUsuario' => $this->auth_model->hashPassword($contrasenaUsuario));
				$this->db->where('idUsuario', $idUsuario);
				$this->db->update($this->tablas['usuario'], $arrNewPass);
				return true;
				// die('cambio');
			} else {

				return false;
				// die('no usuario');
			}

		}

	}

	function cambiar_email($idUsuario, $emailUsuario) {

		if (!$this->is_there_emailUsuario($emailUsuario['emailUsuario'])) {

			$this->db->where('idUsuario', $idUsuario);
			$this->db->update($this->tablas['usuario'], $emailUsuario);
			return true;
		} else {

			return false;
		}

	}

	function addDatosCurriculum($arrInsert) {

		$this->db->insert($this->tablas['candidato'], $arrInsert);
		return true;
	}

	//      Metodos para manejo de todos los Usuarios

	function is_there_usuario($usuario) {

		//verifica si un usuario ya existe antes de registrarlo

		$this->db->where('usuario', $usuario);
		$query = $this->db->get($this->tablas['usuario']);
		// $query = $this->db->get('usuario');
		if ($query->num_rows() == 1)

			return true;
		return false;
	}

	function is_there_emailUsuario($emailUsuario) {

		//verifica si un email ya existe antes de registrarlo

		$this->db->where('emailUsuario', $emailUsuario);
		$query = $this->db->get($this->tablas['usuario']);
		// $query = $this->db->get('usuario');
		if ($query->num_rows() == 1)

			return true;
		return false;
	}

	/*Administración de usuarios*/

	function listAllUsers($roles = null) {
		$this->db->select($this->tablas['usuario'] . '.nombre, ' . $this->tablas['usuario'] . '.apellidos, ' . $this->tablas['usuario'] . '.usuario, ' . $this->tablas['usuario'] . '.emailUsuario, ' . $this->tablas['usuario'] . '.idRol, ' . $this->tablas['usuario'] . '.idUsuario, '.$this->tablas['rol'].'.idRol, '.$this->tablas['rol'].'.nombreRol');
		$this->db->join($this->tablas['rol'],$this->tablas['rol'].'.idRol = '.$this->tablas['usuario'].'.idRol','left', false);
		$this->db->from($this->tablas['usuario']);	
		if ($roles != null)
			$this->db->where_in($this->tablas['usuario'] . '.idRol', $roles);		$query = $this->db->get();		if ($query->num_rows() > 0)
		return $query->result();		return null;
	}

	function userToEmpresa($idUsuario, $idEmpresa, $tipoUsuario) {

		$arr = array('idusuario' => $idUsuario, 'idEmpresa' => $idEmpresa, 'tipoUsuario' => $tipoUsuario);
		$this->db->insert($this->tablas['usuarioempresa'], $arr);
		return true;
	}

	function deleteUser($idUsuario) {

		$this->db->where('idUsuario', $idUsuario);
		$this->db->delete($this->tablas['usuario']);
		return true;
	}

	function banearUser($idUsuario, $currentStatus) {

		$status = '';
		$this->db->where('idUsuario', $idUsuario);
		if ($currentStatus == 1) {

			$this->db->update($this->tablas['usuario'], array('status' => 2));
			$status = 'bannedOK';
		} else if ($currentStatus == 2) {

			$this->db->update($this->tablas['usuario'], array('status' => 1));
			$status = 'unblockedUser';
		}

		return $status;
	}

}
