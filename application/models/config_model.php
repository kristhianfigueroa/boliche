<?php
 class Config_model extends CI_Model {

	var $tablas = array();
	function __construct() {
		parent::__construct();
		$this->load->config('tables', TRUE);
		$this->tablas = $this->config->item('tablas', 'tables');
	}

	public function getCostosxhora()
	{
		$query = $this->db->get($this->tablas['costoxhora']);
		return ($query->num_rows()>0)?$query->result():null;
	}

	public function insertCosto($arrInsert){
		$this->db->insert($this->tablas['costoxhora'],$arrInsert);
		$idCosto = $this->db->insert_id();
		return $this->db->get_where($this->tablas['costoxhora'],array('idCosto'=>$idCosto))->row();
	}

	function deleteCosto($idCosto){
		return $this->db->delete($this->tablas['costoxhora'],array('idCosto'=>$idCosto));
	}


	public function getMotivosRechazo()
	{
		$query = $this->db->get($this->tablas['motivosrechazocotizacion']);
		return $query->result();
	}

	public function getAdquisiciones()
	{
		$query = $this->db->get($this->tablas['adquisicioncliente']);
		return $query->result();
	}

	public function getEstatusCliente()
	{
		$query = $this->db->get($this->tablas['estatuscliente']);
		return $query->result();
	}

}