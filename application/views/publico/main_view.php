<!DOCTYPE html>
<html lang="es">
    <head>
       
        <meta charset="utf-8" />
        <title><?=$SYS_metaTitle?></title>
        <meta name="keywords" content="<?=$SYS_metaKeyWords?>">
        <meta name="description" content="<?=$SYS_metaDescription?>">
        <link href="<?=base_url().IMG_DIR?>favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" href="<?=base_url().CSS_DIR?>layout.css" type="text/css">
        <link rel="stylesheet" href="<?=base_url().CSS_DIR?>validator/validationEngine.jquery.css" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
        
        
        <!-- GENERAL SCRIPTS -->    
        <script type="text/javascript" src="<?=base_url().JS_DIR?>jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="<?=base_url().JS_DIR?>validator/languages/jquery.validationEngine-es.js"></script>
        <script type="text/javascript" src="<?=base_url().JS_DIR?>validator/jquery.validationEngine.js"></script>
        <script type="text/javascript">


            jQuery(document).ready(function($) {
                $('body').animate({
                    opacity:1
                },{duration:1000})

            });
        </script>
    </head>

    <body>
        
        
            <?php $this->load->view($module)?>
        
    </body>
</html>