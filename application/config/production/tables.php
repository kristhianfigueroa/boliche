<?php
/*
 * 
 * TABLAS NECESARIAS
 * 
 */
$config['tablas']['usuario']                  = 'sys_usuario';
$config['tablas']['permiso']                  = 'sys_permiso';
$config['tablas']['rol']                      = 'sys_rol';
$config['tablas']['roltienepermiso']          = 'sys_roltienepermiso';

$config['tablas']['cliente']                  = 'erp_cliente';
$config['tablas']['contactocliente']          = 'erp_contactocliente';
$config['tablas']['comentariocliente']        = 'erp_comentariocliente';
$config['tablas']['comentariocliente']        = 'erp_comentariocliente';
$config['tablas']['cotizacion']  		      = 'erp_cotizacion';
$config['tablas']['conceptocotizacion']       = 'erp_conceptocotizacion';
$config['tablas']['costoxhora'] 		      = 'erp_catalogo_costoxhora';
$config['tablas']['motivosrechazocotizacion'] = 'erp_catalogo_motivorechazocotizacion';
$config['tablas']['costoextracotizacion']     = 'erp_costoextracotizacion';
$config['tablas']['adquisicioncliente'] 	  = 'erp_adquisicioncliente';
$config['tablas']['estatuscliente'] 	      = 'erp_estatuscliente';