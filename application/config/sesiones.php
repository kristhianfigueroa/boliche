<?php
// Acá configuramos cualquier campo de cualquier tabla que queramos que se cargue en nuestras variables de sesión
// Tienen qu coincidir con las tablas del archivo tables.


//cambia a true si quieres que se active la función
$config['custom_sess'] = false;

//Sólo se tiene que agregar líneas como la siguiente, donde se establece en un array la tabla de la cual se van a obtener
//y el campo. En la sessión se creará $this->session->set_userdata(array('campo'=>'valor'))

//NOTA La tabla, tiene que tener un campo idUsuario, para que se pueda ligar con cada usuario al cargar la sesión.

$config['arr_custom_sess'][] = array('tabla'=>'tabla_a_cargar','campo'=>'campo_a_cargar');

?>