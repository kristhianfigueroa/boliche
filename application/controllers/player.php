<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Player extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('player_model');
	}

	public function index()
	{
		$data['SYS_metaTitle']          = SITE_NAME;
		$data['SYS_metaDescription']    = '';
		$data['SYS_metaKeyWords']       = '';
		$data['pestana'] 				= 'home';
	    $data['module']					= 'publico/player/list_view';
        
		
		$data['css'] = array();
		
		$data['js'] = array();

		$data['players'] = $this->player_model->getPlayers();
		
		$this->load->view('publico/main_view', $data);
	}

	public function create()
	{
		$data['SYS_metaTitle']          = SITE_NAME;
		$data['SYS_metaDescription']    = '';
		$data['SYS_metaKeyWords']       = '';
		$data['pestana'] 				= 'home';
	    $data['module']					= 'publico/player/create_view';
        
		
		$data['css'] = array();
		
		$data['js'] = array();
		
		$this->load->view('publico/main_view', $data);
	}

	public function create_do()
	{
		$arrInsert = array();
		foreach($this->input->post() as $k=>$v){
			if($k!='nombre_imagen'){
				$arrInsert[$k] = $v;
			}
		}
		$target = 'imagen_usuario';
		$img_data = array(
			'date'    => true,
			'random'  =>  true,
			'user_id' => null,
			'width'   => null,
			'height'  => null
		);
		$this->load->model('file_model');
		$imagen = $this->file_model->uploadItem($target,$img_data,'nombre_imagen',false,$this->input->post('usuario'));
		if(!is_array($imagen)){
			$arrInsert['nombre_imagen'] = $imagen;
			$this->player_model->insertPlayer($arrInsert);
		}
		else{
			die(var_dump($imagen));
		}
		redirect('player');
	}

}

/* End of file player.php */
/* Location: ./application/controllers/player.php */