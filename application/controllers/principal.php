<?php if ( ! defined('BASEPATH')) 
	exit('No direct script access allowed');

class Principal extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
    }
	
	function index() {	    
        $data['SYS_metaTitle']          = SITE_NAME;
		$data['SYS_metaDescription']    = '';
		$data['SYS_metaKeyWords']       = '';
		$data['pestana'] 				= 'home';
	    $data['module']					= 'publico/principal_view';
        
		
		$data['css'] = array();
		
		$data['js'] = array();
		
		$this->load->view('publico/main_view', $data);
	}

	function create($value='')
	{
		# code...
	}

}
