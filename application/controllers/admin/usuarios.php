<?php

if (!defined('BASEPATH'))

	exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	function __construct() {

		parent::__construct();

		if (!is_logged()){
			//checamos si existe una sesión activa
			$query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
			$redir = str_replace('/', '-', uri_string().$query);
			$this->session->keep_flashdata('error');
			redirect('admin/login/index/' . $redir);
		}// 
		// if (!is_authorized(array(0, 1), null, $this -> session -> userdata('nivel'), $this -> session -> userdata('rol'))) {
		// 	$this -> session -> set_flashdata('error', 'userNotAutorized');
		// 	redirect('admin');
		// }
		$this->load->model('usuario_model');
	}

	public function index() {

		$data['SYS_MetaTitle'] = 'Dashboard | Administración '.getSitio();

		$data['SYS_MetaDescription'] = " 'Creacion de marcas, querétaro";

		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 'usuarios';
		$data['subPestana'] = 'usuarios';

		$data['usuarios'] = $this->usuario_model->listAllUsers();

		$data['modulo'] = 'admin/usuarios/list_view';
		$this->load->view('admin/main_view', $data);

	}

	public function nuevo(){
		$data['SYS_MetaTitle'] = 'Dashboard | Administración '.getSitio();

		$data['SYS_MetaDescription'] = "";

		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 'usuarios';
		$data['subPestana'] = 'nuevousuario';

		$data['js'] = array(
			'fancybox/jquery.fancybox-1.3.4.pack',
			'validation/languages/jquery.validationEngine-es',
			'validation/jquery.validationEngine'

		);
		$data['css'] = array(
			'fancybox/jquery.fancybox-1.3.4',
			'validation/validationEngine.jquery',
			'validation/template'
		);

		$data['permisos'] = $this->usuario_model->getPermisos();

		$data['modulo'] = 'admin/usuarios/agregar_usuario_view';
		$this->load->view('admin/main_view', $data);
	}

	function nuevo_do(){
		$data = array();
			$data = array();
		foreach($this->input->post() as $k=>$v){
			if($k!='contrasenaUsuario2'&&$k!=='permisos'&&$k!='todos'){
				$data[$k] = $v;
			}
		}

		$data['nivel'] = 1;
		$data['status'] = 1;

		$idUsuario = $this->usuario_model->registrarUsuario($data);

		$arrPermisos = $this->input->post('permisos');

		if($arrPermisos!=false){
			foreach($arrPermisos as $row){
				$this->usuario_model->insertUsuariotienepermiso(array('idUsuario'=>$idUsuario,'idPermiso'=>$row));
			}
		}

		$this->session->set_flashdata('error', 'insertOk');
		redirect('admin/usuarios');

	}

	public function editar($idUsuario){
		$data['SYS_MetaTitle'] = 'Dashboard | Administración '.getSitio();

		$data['SYS_MetaDescription'] = "";

		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 'usuarios';
		$data['subPestana'] = 'usuarios';

		$data['usuario'] = $this->usuario_model->getUsuario($idUsuario);
		$permisos = $this->usuario_model->getPermisosUsuario($idUsuario);
		$data['usuariotienepermiso'] = array();
		if($permisos!=null){
			foreach($permisos as $permiso){
				$data['usuariotienepermiso'][] = $permiso->idPermiso;
			}
		}
		
		$data['js'] = array(
			'fancybox/jquery.fancybox-1.3.4.pack',
			'validation/languages/jquery.validationEngine-es',
			'validation/jquery.validationEngine'

		);
		$data['css'] = array(
			'fancybox/jquery.fancybox-1.3.4',
			'validation/validationEngine.jquery',
			'validation/template'
		);

		$data['modulo'] = 'admin/usuarios/editar_view';
		$this->load->view('admin/main_view', $data);
	}

	public function editar_do($idUsuario){
		$data = array();
		foreach($this->input->post() as $k=>$v){
			if($k!=='contrasenaUsuario'&&$k!=='contrasenaUsuario2'&&$k!=='permisos'&&$k!='todos'){
				$data[$k] = $v;
			}
		}

		$arrPermisos = $this->input->post('permisos');

		if($arrPermisos!=false){
			$this->usuario_model->deletePermisos($idUsuario);
			foreach($arrPermisos as $row){
				$this->usuario_model->insertUsuariotienepermiso(array('idUsuario'=>$idUsuario,'idPermiso'=>$row));
			}
		}

		if($this->input->post('contrasenaUsuario')!=false){
			$this->usuario_model->cambiarContrasena(null, $this->input->post('contrasenaUsuario'), $idUsuario, true);
		}

		$this->usuario_model->editarUsuario($idUsuario,$data);
		
		$this->session->set_flashdata('error', 'updateOk');
		redirect('admin/usuarios/editar/'.$idUsuario);
	}

	function deleteUser()
	{
		$this->usuario_model->deleteUser($this->input->post('idUsuario'));
		$data['response'] = 'true';
		$data['return'] = $this->lang->line('deleteOk');
		echo json_encode($data);
	}

	function deleteGaleria(){
		$this->usuario_model->deleteGaleria($this->input->post('idUsuario'));
		$data['response'] = 'true';
		$data['return'] = $this->lang->line('deleteOk');
		echo json_encode($data);
	}

	public function status(){
		$data = array(
			'publicado' => $this->input->post('publicado')
		);
		$this->usuario_model->updateGaleria($this->input->post('idUsuario'),$data);
		$data['response'] = 'true';
		$data['return'] = $this->lang->line('updateOk');
		echo json_encode($data);
	}

	function deleteImg(){
		$this->usuario_model->deleteImgGaleria($this->input->post('idImg'));
		$data['response'] = 'true';
		echo json_encode($data);
	}

}