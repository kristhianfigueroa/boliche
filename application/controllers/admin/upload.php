<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('file_model');
	}

	function uploadImg($target,$name=null,$for_crop=null){
		$data = array(
			'date'    => false,
			'random'  => true,
			'user_id' => null
		);
		$target = str_replace('-', '/', $target);
		if(is_null($name)||$name=="null"){
			$name = null;
		}
		if(is_null($for_crop)||$for_crop=="null"){
			$for_crop = null;
		}
		$imagen = $this->file_model->uploadify($target,'image',$data,$name,$for_crop);
		if(!is_array($imagen)){
			echo $imagen;
		}
		else{
			echo $imagen['error'];
		}
	}

	function uploadFile($target){		
		$data = array(
			'date'    => false,
			'random'  => true,
			'user_id' => null
		);
		$target = str_replace('-', '/', $target);
		$imagen = $this->file_model->uploadify($target,'doc',$data);
		$data = array();
		if(!is_array($imagen)){
			// $data['file'] = $imagen;
			// $data['response'] = 'ok';
			echo $imagen;
		}
		else{
			// $data['file'] = '';
			// $data['response'] =  $imagen['error'];
			echo 'err';
		}
		// echo json_encode($data);
	}

	function crop($folder,$img_url){
		if($this->input->get('ratio')!=false){
			$data['ratio'] = $this->input->get('ratio');
		}
		else{
			$data['ratio'] = '10 / 7';
		}
		if($this->input->get('w')!=false){
			$data['nw'] = $this->input->get('w');
		}
		else{
			$data['nw'] = 500;
		}
		if($this->input->get('h')!=false){
			$data['nh'] = $this->input->get('h');
		}
		else{
			$data['nh'] = 350;
		}
		$data['img_url'] = $folder."/".$img_url;
		$data['filename'] = $img_url;
		$data['folder'] = $folder;
		$this->load->view('admin/crop/crop_view',$data);
	}

	function crop_do()
	{
		 $arrCropDimensions = array(
                'x'  => $this->input->post('x'),
                'y'  => $this->input->post('y'),
                // 'w'  => (int)$this->input->post('w')+9,
                // 'h'  => (int)$this->input->post('h')+9,
                'w'  => (int)$this->input->post('w'),
                'h'  => (int)$this->input->post('h'),
                'nw' => $this->input->post('nw'),
                'nh' => $this->input->post('nh')
        );

        $filename = $this->input->post("filename");
        $folder = $this->input->post('folder');
        
        $new_img = $this->file_model->cropImage($arrCropDimensions, $filename, 'docs/'.$folder.'/', 'docs/'.$folder.'/');
        echo $new_img;
	}
}

/* End of file upload.php */
/* Location: ./application/controllers/upload.php */