<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sistema extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!is_logged()){
			//checamos si existe una sesión activa
			$query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
			$redir = str_replace('/', '-', uri_string().$query);
			$this->session->keep_flashdata('error');
			redirect('admin/login/index/' . $redir);
		}// 
		if (!is_authorized(array(0, 1), null, $this -> session -> userdata('nivel'), $this -> session -> userdata('rol'))) {
			$this -> session -> set_flashdata('error', 'userNotAutorized');
			redirect('admin');
		}
		$this->load->model('usuario_model');
		$this->load->model('config_model');
	}

	public function index()
	{
		redirect('admin');
	}

	public function costoxhora()
	{
		parse_str($_SERVER['QUERY_STRING'],$_GET);

		$data['SYS_MetaDescription'] = "Inweb Lab";

		$data['SYS_MetaKeywords'] = 'Inweb Lab';
		$data['pestana'] = "crm";
		$data['subPestana'] = 0;
		$data['modulo'] = 'admin/sistema/costoxhora_view';
        $data['SYS_MetaTitle'] = 'Costos por hora | '.getSitio();
        $data['header_title'] = 'Costos por hora';

        $data['costos'] = $this->config_model->getCostosxhora();

        $data['js'] = array(
			'validator/languages/jquery.validationEngine-es',
			'validator/jquery.validationEngine'
		);
		$data['css'] = array(
			'validator/validationEngine.jquery',
			'validator/template'
		);

		$this->load->view('admin/main_view', $data);
	}

	public function nuevo_costo_do(){

		$data = array(
			'identificador' => $this->input->post('identificador'),
			'costo'         => $this->input->post('costo')

		);
		
		$costo = $this->config_model->insertCosto($data);
		$data_return = array(
			'response'      => "true",
			'idCosto'       => $costo->idCosto,
			'identificador' => $costo->identificador,
			'costo'         => $costo->costo
		);
		echo json_encode($data_return);
	}

	public function deleteCosto(){
		$this->config_model->deleteCosto($this->input->post('idCosto'));
		$data['response'] = 'true';
		$data['return'] = $this->lang->line('deleteOk');
		echo json_encode($data);
	}

}

/* End of file sistema.php */
/* Location: ./application/controllers/sistema.php */